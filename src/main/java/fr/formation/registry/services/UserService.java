package fr.formation.registry.services;

import java.util.Collection;
import java.util.Optional;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import fr.formation.registry.models.User;
import fr.formation.registry.repositories.UserRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class UserService {

	private final UserRepository userRepository;

	private PasswordEncoder passwordEncoder;
	
	@PreAuthorize("isAuthenticated()")
	public Collection<User> findAll() {
		return userRepository.findAll();
	}

	public Optional<User> findByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@PreAuthorize("hasRole('ROLE_ADMIN') || #user.role == T(fr.formation.registry.models.UserRole).USER")
	public User save(User user) {
		user.setId(0L);
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		return userRepository.save(user);
	}

	
}
