package fr.formation.registry.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import fr.formation.registry.models.User;
import fr.formation.registry.services.UserService;
import lombok.AllArgsConstructor;


@Service
@AllArgsConstructor
public class MyUserDetailsService implements UserDetailsService {

	private UserService userService;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User u = userService.findByEmail(username).orElseThrow(() -> new UsernameNotFoundException("email " + username + "not found"));
		return new MyUserDetails(u);
	}

}
