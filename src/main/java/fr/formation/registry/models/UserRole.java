package fr.formation.registry.models;

public enum UserRole {
    USER,
    ADMIN
}
