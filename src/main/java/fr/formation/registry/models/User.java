package fr.formation.registry.models;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@Data
@SuperBuilder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "people")
public class User {
    
	@Id
	@GeneratedValue
	@EqualsAndHashCode.Include
	private long id;
	
	@Column(unique = true)
	@Email
	private String email;

    @NotBlank
    @Length(min = 8)
	private String password;

	private UserRole role;

	@Builder.Default
	@OneToMany(mappedBy = "owner")
	@JsonIgnore
	private Set<Address> addresses = new HashSet<>();
}
