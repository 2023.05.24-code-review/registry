package fr.formation.registry.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@NoArgsConstructor
@Data
@SuperBuilder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class Address {
    
	@Id
	@GeneratedValue
	@EqualsAndHashCode.Include
	private long id;
	
    @NotBlank
	private String city;

    @ManyToOne
	private User owner;
}
