package fr.formation.registry.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.formation.registry.models.Address;

public interface AddressRepository extends JpaRepository<Address, Long> {
    
}
